import React, {Component} from 'react';

class Filter extends React.Component {
    render() {
        return (
            <div className='filter'>
                <div className='filter-result'>{this.props.count} Product</div>
                <div className='filter-sort'>Order {''} <select value={this.props.sort}
                                                                onChange={this.props.sortProducts}>
                    <option>Latests</option>
                    <option value="Lowest">Lowest</option>
                    <option value="Highest">Highest</option>
                </select>
                </div>

                <div className='filter-size'>Filter {''} <select value={this.props.size}
                                                                 onChange={this.props.filterProducts}>
                    <option value="X">X</option>
                    <option value="L">L</option>
                    <option value="XXL">XXL</option>
                    <option value="XL">Xl</option>
                </select></div>
            </div>
        );
    }
}

export default Filter;