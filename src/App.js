import {BrowserRouter, Route} from 'react-router-dom'
import data from './data.json'
import React from 'react'
import Products from "./component/Products";
import Filter from "./component/Filter/Filter";
import Cart from "./component/Cart/cart";


class App extends React.Component {
    constructor() {
        super();
        this.state = {
            products: data.products,
            cartItems:[],
            size: "",
            sort: "",
        }
    }
    //Удаление Товара с Корзины
    removeFromCart = (product) => {
        const cartItems = this.state.cartItems.slice()
        this.setState(({cartItems:cartItems.filter(x=>x._id !== product._id)}))

    }
    //Добавление товара в корзину
    addToCart = (product) => {
        const cartItems = this.state.cartItems.slice();
        let alreadyInCart = false
        cartItems.forEach(item => {
            if (item._id === product._id){
                item.count++;
                alreadyInCart = true;
            }
        });
        if (!alreadyInCart){
            cartItems.push({...product, count: 1});
        }
        this.setState({cartItems })
    }
    //Филтрация по ценам
    sortProducts(event) {
        const sort = event.target.value
        console.log(event.target.vatlue)
        this.setState((state) => ({
            sort: sort,
            products: this.state.products.slice().sort((a, b) =>
            sort === 'Lowest'
            ?a.price < b.price
            ?1
            :1
            :sort === 'Highest'
            ?a.price > b.price
            ? 1
            : -1
            :a._id > b._id
            ? 1
            : -1),
        }))
    }
    //Фильтрация по размерам
    filterProducts = (event) => {
        console.log(event.target.value)
        if (event.target.value === '') {
            this.setState({size: event.target.value, product: data.products})
        } else {
            this.setState({
                size: event.target.value,
                products: data.products.filter(product => product.availableSize.indexOf(event.target.value) >= 0)
            })
        }


    }

    render() {
        return (
            <BrowserRouter>
                <div className='grid-container'>
                    <header>
                        <a href="/">React Shopping Cart</a>
                    </header>
                    <main>
                        <div className='content'>
                            <div className='main'>
                                <Filter count={this.state.products.length}
                                        size={this.state.size}
                                        sort={this.state.sort}
                                        filterProducts={this.filterProducts}
                                        sortProducts={this.sortProducts}/>
                                <Products products={this.state.products}
                                          addToCart = {this.addToCart}/>
                            </div>
                            <div className='sidebar'>
                                <Cart cartItems = {this.state.cartItems} removeFromCart = {this.removeFromCart}/>
                            </div>
                        </div>
                    </main>
                    <footer>
                        All right is reserved
                    </footer>
                </div>
            </BrowserRouter>
        )
    }
}

export default App;
